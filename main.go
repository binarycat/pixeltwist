package main

import (
	"image"
	"image/draw"
	png "image/png"
	_ "image/jpeg"
	"os"
	//"log"
)

// some curves are stateful, some are not.
type Curve func(image.Rectangle) func(int) image.Point

var curvetbl = map[string]Curve{
	"row": RowCurve,
	"col": ColCurve,
	"spiral": SpiralCurve,
}

func RowCurve(r image.Rectangle) func(n int) image.Point {
	return func(n int) image.Point {
		span := r.Max.X - r.Min.X
		return image.Pt(r.Min.X + n % span, r.Min.Y + n / span)
	}
}

func ColCurve(r image.Rectangle) func(n int) image.Point {
	return func(n int) image.Point {
		span := r.Max.Y - r.Min.Y
		return image.Pt(r.Min.X + n / span, r.Min.Y + n % span)
	}
}

func SpiralCurve(r image.Rectangle) func(n int) image.Point {
	mode := 0
	pt := r.Min
	box := r.Inset(1)
	return func(n int) image.Point {
		res := pt
		//log.Printf("pt=%v", pt)
		switch mode {
		case 0:
			pt.X += 1
			if pt.X >= box.Max.X { mode += 1 }
		case 1:
			pt.Y += 1
			if pt.Y >= box.Max.Y { mode += 1 }
		case 2:
			pt.X -= 1
			if pt.X <= box.Min.X { mode += 1 }
		case 3:
			pt.Y -= 1
			if pt.Y <= box.Min.Y {
				mode = 0
				box = box.Inset(1)
			}
		}
		return res
	}
}

func main() {
	imgi, _, err := image.Decode(os.Stdin)
	if err != nil { panic(err) }
	rect := imgi.Bounds()
	curvei := curvetbl[os.Args[1]](rect)
	curveo := curvetbl[os.Args[2]](rect)
	imgo := image.NewRGBA(rect)
	npixel := rect.Dx() * rect.Dy()
	for i := 0; i < npixel; i++ {
		pti := curvei(i)
		pto := curveo(i)
		draw.Draw(imgo, image.Rect(pto.X, pto.Y, pto.X+1, pto.Y+1), imgi, pti, draw.Src)
	}
	png.Encode(os.Stdout, imgo)
}
